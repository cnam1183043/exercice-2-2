package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import java.io.IOException;
import java.io.InputStream;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

public interface DatabaseSerializer <M extends ImageFrameMedia> extends BadgeDeserializer{
    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param media
     * @param <K>
     * @return
     * @throws IOException
     */
    <K extends InputStream> K getDeserializingStream(M media) throws IOException;
}
