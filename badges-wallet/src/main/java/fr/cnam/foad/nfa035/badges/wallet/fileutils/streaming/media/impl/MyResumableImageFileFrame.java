package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

public class MyResumableImageFileFrame extends AbstractImageFrameMedia<File>  implements ResumableImageFrameMedia{

	private BufferedReader reader;
	
    public MyResumableImageFileFrame(File walletDatabase) {
        super(walletDatabase);
    }
	
	@Override
	public File getChannel() {
		// TODO Auto-generated method stub
		return super.getChannel();
	}

	@Override
	public InputStream getEncodedImageInput() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream getEncodedImageOutput() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (reader == null || !resume){
            this.reader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel())));
        }
        return this.reader;
	}

}
