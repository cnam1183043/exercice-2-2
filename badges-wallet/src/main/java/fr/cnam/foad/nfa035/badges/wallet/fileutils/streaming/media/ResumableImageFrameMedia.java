package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

public interface ResumableImageFrameMedia extends ImageFrameMedia{
	/**
	 * 
	 * @param resume
	 * @return
	 * @throws IOException
	 */
	public BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
