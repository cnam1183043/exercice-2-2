package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import java.io.IOException;
import java.io.OutputStream;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

public interface BadgeDeserializer <M extends ImageFrameMedia> {
    /**
     * Désérialise une image depuis un media quelconque vers un support quelconque
     *
     * @param media
     * @throws IOException
     */
    public void deserialize(M media) throws IOException;
    
    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @param <T>
     * @return
     */
    public <T extends OutputStream> T getSourceOutputStream();

	/**
	 * Affecte un outputstream
	 * @param <T>
	 * @param <T>
	 * @param os
	 */
	public <T extends OutputStream> void setSourceOutputStream(T os);
}
