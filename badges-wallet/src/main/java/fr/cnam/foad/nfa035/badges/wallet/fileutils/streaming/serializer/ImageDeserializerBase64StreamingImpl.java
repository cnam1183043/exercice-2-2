package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer{

    private OutputStream sourceOutputStream;

    /**
     * {@inheritDoc}
     *
     * @return le flux d'écriture
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    
    /**
     * 
     */
	@Override
	public InputStream getDeserializingStream(ImageFrameMedia media) throws IOException {
		// TODO Auto-generated method stub
		return null;
		//return new Base64InputStream(media.getEncodedImageInput());
	}


}
