package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;

import org.apache.commons.codec.binary.Base64OutputStream;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

public class ImageSerializerBase64DatabaseImpl extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {
	
    @Override
	public InputStream getSourceInputStream(File source) throws FileNotFoundException { return new FileInputStream(source); }
	
	 @Override
	 public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
		 return new Base64OutputStream(media.getEncodedImageOutput());
	 }
	 
	    @Override
	    public final void serialize(File source, ImageFrameMedia media) throws IOException {
	        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
	        long size = Files.size(source.toPath());
	        try(OutputStream os = media.getEncodedImageOutput()) {
	            Writer writer = new OutputStreamWriter(os);
		            try(OutputStream eos = getSerializingStream(media)) {
	                getSourceInputStream(source).transferTo(eos);
	                writer.write("\n");
	            }
	            writer.flush();
	        }
	    }
}
