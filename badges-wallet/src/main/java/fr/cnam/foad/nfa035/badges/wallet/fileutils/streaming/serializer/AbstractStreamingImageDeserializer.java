package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import java.io.IOException;
import java.io.OutputStream;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

/**
 * Classe abstraite visant à structurer/guider le développement de manière rigoureuse
 * Elle s'applique donc à tout objet dont les methodes auraient pour effet la sérialisation d'une image,
 * quel qu'en soit le format ou le média/canal de destination.
 * 
 * @param <M> Le Media de sérialisation,
 *           à partir duquel nous voulons désérialiser notre image en base 64,
 *           c'est-à-dire la récupérer à son format original.
 */
public abstract class AbstractStreamingImageDeserializer<M extends ImageFrameMedia, T extends OutputStream> implements ImageStreamingDeserializer<M> {
	
	private OutputStream sourceOutputStream;
	
    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @param <T>
     * @return
     */
    public OutputStream getSourceOutputStream() {
    	return sourceOutputStream;
    }
  
    
    /**
     * Affecte un outputstream
     * @param <T>
     * @param <T>
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
    	sourceOutputStream = os;
    }
	
    /**
     * Désérialise une image depuis un media quelconque vers un support quelconque
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}
